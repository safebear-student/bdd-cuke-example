package com.safebear.app;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by sstratton on 08/05/17.
 */
public class Test03_MainPageJavascriptInputWithWait extends BaseTest {

    /**
     * Copyright safebear Ltd 2017
     *
     * This test will click on the 'Say Something' button, explicitly wait for the pop-up to appear,
     * enter some text into the pop-up and check the result. See Steps below.
     */

    @Test
    public void testJavaScriptInputWithWait() {
//        Step 1 Confirm we're on the Welcome Page by checking the page title
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login and confirm we've changed to the Login Page
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
//        Step 3 Log in and confirm we're now on the User Page
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
//        Step 4 click on the JavaScript Input button and explicitly wait for an alertbox to appear
        assertTrue(userPage.clickJavascriptInput());
//        Step 5 Enter "Hello" in the alert box and confirm this is return back onscreen
//        assertTrue(userPage.enterTextInAlert("Hello"));
    }

}
