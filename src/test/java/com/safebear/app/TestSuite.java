package com.safebear.app;

/**
 * Created by sstratton on 08/05/17.
 */


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Copyright safebear Ltd 2017
 *
 * This is where you list the order in which you want your tests to run.
 *
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Test01_Login.class,
        Test02_LoginPageMultiElements.class,
        Test03_MainPageJavascriptInputWithWait.class,
        Test04_MainPageFrames.class,
        Test05_MainPageDropDowns.class
})

public class TestSuite {
}
