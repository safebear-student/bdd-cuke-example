package com.safebear.app;

import com.safebear.app.pages.FramesPage;
import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.Utils;
import cucumber.annotation.After;
import cucumber.annotation.Before;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;
import cucumber.runtime.PendingException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

public class StepDefs{


    WebDriver driver;
    Utils utility;
    protected LoginPage loginPage;
    protected WelcomePage welcomePage;
    protected UserPage userPage;
    protected FramesPage framesPage;
    String speak;
    String onscreen;

    @Before
    public void setUp() throws MalformedURLException {

//        Decide which driver you're using
        driver = new ChromeDriver();

//        Code for running headless if you have the selenium server up and running
//        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnitWithJs();
//        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);

//        Implicit wait
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        Maximise your window
        driver.manage().window().maximize();

//        Create PageObjects
        utility = new Utils();
        loginPage = new LoginPage(driver);
        welcomePage = new WelcomePage(driver);
        userPage = new UserPage(driver);
        framesPage = new FramesPage(driver);

    }


    @After
    public void tearDown(){
//        I put a delay of 4 seconds in here so you can see for yourself if the tests are successful
//        or not
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }


    @Given("^that I (?:login|log in) with (.+) and (.+)$")
    public void that_I_log_in_with_valid_credentials(String username, String password, String doc) throws Throwable {
        // Express the Regexp above with the code you wish you had
        // Go to the website using our Utils method
        assertTrue(utility.navigateToWebsite(driver));
        //        Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login link at top of page and confirm we move to the Login Page
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
//        Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,username, password));
        System.out.println("\n" + doc + "\n" + username + " / " + password + "\n" + "\n");
    }

    @When("^I say (.+)$")
    public void I_say_something(String speak, String doc) throws Throwable {
        this.speak = speak;
//        Step 4 click on the JavaScript Input button and explicitly wait for an alertbox to appear
        assertTrue(userPage.clickJavascriptInput());
//        Step 5 Enter "Hello" in the alert box and confirm this is return back onscreen
        this.onscreen = userPage.enterTextInAlert(speak);
        System.out.println(doc + "\n" + "\"" + speak + "\"" + "\n");
    }

    @Then("^it appears on the userpage$")
    public void it_appears_on_the_userpage(String doc) throws Throwable {
        // Express the Regexp above with the code you wish you had
        assertThat(this.onscreen,containsString(this.speak));
        System.out.println(doc + "\n" + "\"" + this.onscreen + "\"" + "\n");
    }


}
