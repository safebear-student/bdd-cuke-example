Feature: SaySomething

  In order to communicate with other users
  As a valid user
  I want to be able to say something on the website

  Rules (acceptance criteria):
   - Must be a valid user
   - Must be visible on the page
   - Must be visible to all users - don't worry about this yet

  To do:
   - Make the message visible to all users.


  Scenario Outline: Valid user says something
    Given that I log in with <username> and <password>
    """
    Given that I have logged in with the following credentials:
    """
    When I say <something>
    """
    When I say this on the website:
    """
    Then it appears on the userpage
    """
    Then what I have said appears on the page:
    """

    Examples:
    |username|password|something|
    |testuser|testing |hello    |